<?php
/**
 * @file
 * secondary_menu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function secondary_menu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-secondary-links.
  $menus['menu-secondary-links'] = array(
    'menu_name' => 'menu-secondary-links',
    'title' => 'Secondary Links',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Secondary Links');


  return $menus;
}
