<?php
/**
 * @file
 * na_events.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function na_events_default_rules_configuration() {
  $items = array();
  $items['rules_unpublish_node'] = entity_import('rules_config', '{ "rules_unpublish_node" : {
      "LABEL" : "Unpublish Node",
      "PLUGIN" : "rule set",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [ { "node_is_published" : { "node" : [ "node" ] } } ],
            "DO" : [ { "node_unpublish" : { "node" : [ "node" ] } } ],
            "LABEL" : "Unpublish Content Rule"
          }
        }
      ]
    }
  }');
  $items['rules_unpublish_past_events'] = entity_import('rules_config', '{ "rules_unpublish_past_events" : {
      "LABEL" : "Unpublish Past Events",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "rules_scheduler" ],
      "ON" : [ "node_insert", "node_update" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "event" : "event" } } } }
      ],
      "DO" : [
        { "schedule" : {
            "component" : "rules_unpublish_node",
            "date" : [ "node:field-date:value2" ],
            "identifier" : "unpublish [node:nid]",
            "param_node" : [ "node" ]
          }
        }
      ]
    }
  }');
  return $items;
}
