<?php
/**
 * @file
 * na_related_weblinks.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function na_related_weblinks_taxonomy_default_vocabularies() {
  return array(
    'links_category' => array(
      'name' => 'Links Category',
      'machine_name' => 'links_category',
      'description' => 'Related NA sites',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
