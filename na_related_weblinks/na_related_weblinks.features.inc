<?php
/**
 * @file
 * na_related_weblinks.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function na_related_weblinks_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function na_related_weblinks_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function na_related_weblinks_image_default_styles() {
  $styles = array();

  // Exported image style: tiny_40.
  $styles['tiny_40'] = array(
    'name' => 'tiny_40',
    'effects' => array(
      2 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 40,
          'height' => 40,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function na_related_weblinks_node_info() {
  $items = array(
    'weblinks' => array(
      'name' => t('Weblinks'),
      'base' => 'node_content',
      'description' => t('A place to land related links'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
